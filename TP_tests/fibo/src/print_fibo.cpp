#include "Fibo.hpp"
#include <iostream>
#include <assert.h>
#include <string>

int main() {
    for (int i = 0; i < 50; i++) {
        try {
            int n = fibo(i-1);
            std::cout << fibo(i) << std::endl;
        } catch (std::string e) {
            std::cout << e << std::endl;
        }
        //assert(fibo(i) >= 0 && fibo(i) <= fibo(i + 1));
    }
    return 0;
}

