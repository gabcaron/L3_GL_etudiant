#include "Fibo.hpp"
#include <string>

int fibo(int n, int f0, int f1) {
    if(n<0) {
        throw std::string("Erreur: n<0");
    } else {
        if(f0<0 && f1<=0) {
            throw std::string("Erreur : f0<0 ou f1<=0");
        } else {
            if(f0>f1) {
                throw std::string("Erreur : f0>f1");
            } else {
                return n<=0 ? f0 : fibo(n-1, f1, f1+f0);
            }
        }
    }
}

