//
// Created by gab on 27/04/2020.
//

#include "Fibo.hpp"
#include <CppUTest/CommandLineTestRunner.h>
#include <string>

TEST_GROUP(GroupFibo) { };

TEST(GroupFibo, Test_1) {
    CHECK_EQUAL(0, fibo(0));
    CHECK_EQUAL(1, fibo(1));
    CHECK_EQUAL(2, fibo(2));
    CHECK_EQUAL(3, fibo(3));
    CHECK_EQUAL(4, fibo(4));
}

TEST(GroupFibo, Test_2) {
    CHECK_THROWS(std::string, fibo(-1));
}
