function repeatN (n, f) {
    var list = "";
    for(let i=1; i<=n; i++) {
        list += "<li>fibo("+ i + ") = " + f(i) + "</li></ br>";
    }
    return list;
}

function fiboNaive (n) {
    if (n < 2)
        return n;
    else
        return fiboNaive(n-1) + fiboNaive(n-2);
}